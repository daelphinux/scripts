# Scripts

Scripts I use for various things. You can find out more about these scripts by going to https://daelphinux.com/index.php/Scripts. Unless noted otherwise, all of these scripts are released under the MIT License, found below in this file. 

## PortScanner
A simple port Scanner in python designed to be used to teach how portscanning works conceptually

## Pakgen
A .pak generator for PHPBB Smilies. Works by pointing it to a directory with appropriately named image files. 

## Valheim Update Script
A script I have used, that is very simple, to update a valheim server allowing the update to be crond in conjunction with a discord bot

## Valheim Server Installer
A BASH script I wrote to interactively install the Valheim server using SteamCMD for easy administration. 

## MIT License
Copyright 2021 Daelphinux

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
