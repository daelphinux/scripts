# Pakgen - a PHPBB Smiley .pak generator
# Released under the terms of the MIT License
# (C) 2021 Daelphinux

import os
from PIL import Image
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("-d","--directory", help="Full or relative path to the directory containing the images. (Default is '.')")
parser.add_argument("-s","--show", help="True or False value on whether to show in the emoji selector when creating a post. (Default is 'False')", type=bool)
parser.add_argument("-o","--output", help="If set, will save the output to a file named as the parameter for output. (Default prints result to stdout)")
args = parser.parse_args()
smilies = []

try:
    test = args.directory
except NameError:
    directory = r'.'
else: 
    directory = r'' + args.directory

for entry in os.scandir(directory):
    try:
        File = entry.name
        image = Image.open(entry.path)
        Width, Height = image.size

        if (args.show):
            Show = '1'
        else:
            Show = '0'

        Emote = File
        Code = ':' + File.split('.')[0].lower() + ':'
        smilies.append([str(File), str(Width), str(Height), str(Show), str(Emote), str(Code)])
    except:
        continue

try:
    test = args.output
except NameError:
    for smiley in smilies:
        print ('\'' + '\',\''.join(smiley) + '\',')
else:
    with open (args.output, 'w') as pak:
        for smiley in smilies:
            pak.write('\'' + '\',\''.join(smiley) + '\',\n')



