#!/bin/bash

export templdpath=$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=./linux64:$LD_LIBRARY_PATH
export SteamAppId=892970

/home/username/.steam/steamcmd/steamcmd.sh +login anonymous +force_install_dir /home/username/Valheim +app_update 896660 +quit

./valheim_server.x86_64 -name {serverName} -port 2456 -world {worldName} -password {worldPassword} -public 1 > /dev/null &

export LD_LIBRARY_PATH=$templdpath

echo "Valheim server started"

while :
do
TIMESTAMP=$(date '+%Y-%m-%d %H:%M:%S')
    echo "valheim.service: timestamp ${TIMESTAMP}"
    sleep 60
done