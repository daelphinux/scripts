# Valheim Server Installer
(c) Daelphinux 2021
Released under the MIT License (see License section below)

# About
This utility is designed to assist with the installation of Valheim Servers on fresh Ubuntu installs. 

I am going to say that again '''I have only tested this on fresh Ubuntu installs'''. That said, I do not see that it would necessarily conflict with anything on an ubuntu home server for local play or something of that ilk.

# Usage
1. Clone the repo
2. Enter into the repo directory
3. run `chmod +x ValheimSrv.sh`
4. run `sudo ./ValheimSrv.sh`
5. Watch the script. It will prompt you for various interactions
    * You will want to have a user password for a new user, a server name, a world name, and a world password. 

# What it does
This script will
1. Install SteamCMD 
2. Create a `valheim` user on the system to run the Valheim service from. This prevents the root account from being used as the service account. 
3. Install the Valheim server from SteamCMD
4. Create a valheim service with your input
5. Enable that service to run on startup
6. Start that service

Be aware that this script will create a user, that is expected. 

# LICENSE 


Copyright 2021 Daelphinux

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


