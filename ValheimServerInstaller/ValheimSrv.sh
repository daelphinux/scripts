#! /bin/bash
# Valheim Dedicated Server Setup Script
# Creates and configures a dedicated Valheim server interactively
# (c) 2021
# Released under the MIT license

# --- PreRequisites --- 
# Assumes you are using UFW as your Firewall

# Add SteamCMD repos
sudo add-apt-repository multiverse
sudo dpkg --add-architecture i386
sudo apt update

# Install SteamCMD and lib32gcc1
# You will need to interactively accept the Steam license agreement
sudo apt -y install lib32gcc1 steamcmd 

echo "SteamCmd installed!"
echo "---Beginning Valheim User Creation---"
# Create the new Valheim user
useradd -d /home/valheim -m -p $(read -sp "User Password: " pw ; echo $pw | openssl passwd -1 -stdin) valheim
echo $'\n---valheim user has been created.---'
# Move the start_valheim file into the valheim user's home. 
cp start_server.sh /home/valheim/start_server.sh
chown valheim /home/valheim/start_server.sh

# Create the Valheim server directory
mkdir /home/valheim/valheimsrv

# Install valheim server from SteamCMD
steamcmd +login anonymous +force_install_dir /home/valheim/valheimsrv +app_update 896660 validate +exit

# Backups are always a good idea
cp /home/valheim/start_server.sh /home/valheim/valheimsrv/start_server.sh
mv /home/valheim/start_server.sh /home/valheim/start_server.sh.bak


echo "---Beginning Server Configuration---"

# Set custom world variables
read -p "Server Name: " serverName
read -p "World Name: " worldName
read -p "World Password: " worldPassword

sed -i "s/{serverName}/$serverName/" /home/valheim/valheimsrv/start_server.sh
sed -i "s/{worldName}/$worldName/" /home/valheim/valheimsrv/start_server.sh
sed -i "s/{worldPassword}/$worldPassword/" /home/valheim/valheimsrv/start_server.sh

# create the final starter file
cp /home/valheim/valheimsrv/start_server.sh /home/valheim/valheimsrv/start_valheim.sh 
rm /home/valheim/valheimsrv/start_server.sh
chmod +x /home/valheim/valheimsrv/start_valheim.sh

# Open ports 2456-2458 in UFW
ufw allow 2456:2458/udp 
ufw allow 2456:2458/tcp

# make sure valheim owns the server start file
chown valheim /home/valheim/valheimsrv/start_valheim.sh
chown -R valheim /home/valheim/

# move the valheim service file
cp ./valheim.service /etc/systemd/system

# start the valheim service
systemctl start /etc/systemd/system/valheim.service

# enable the valheim service
systemctl enable /etc/systemd/system/valheim.service

# wait fifteen seconds for the service to start
sleep 15s

if [ $(systemctl is-active --quiet service) ]
    then 
        echo '#-#-#-The Valheim service is now running!-#-#-#'
else
    echo '!-!-!-The Valheim service failed to start-!-!-!.'
fi


