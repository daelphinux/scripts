#! /usr/bin/Python

# A Simple PortScanner
# Daelphinux 2021
# Released under the MIT License
# Created as a tutorial on portScanning, not designed for regular use

# Necessary imports
import socket

# Global Variables
networkAddress = "192.168.1.0"
subnet = "24"
ports = [21]


# Functions
def TestSocket (host, port):
    '''
    TestSocket
    A function to attempt to open a socket on a host system at a specific port
    Keyword Arguments: 
        host - the target IP address
        port - the target port
    '''
    # Create the socket
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # Set the timeout 
    s.settimeout(1)
    # Attempt to open a socket against the target at the specified port
    result = s.connect_ex((host,port))
    # Determine the result of the connection
    if result == 0:
        return True
    else:
        return False
    # Close the socket
    # If we do not do this the socket will stay open, and there are a limited number of those. 
    s.close()

def GetInfo():
    '''
    GetInfo
    A function to gather necessary information for scanning from the user. 
    '''
    # Set Globals
    global network
    global networkAddress
    global subnet
    global ports

    # Get the user's definition of the network to scan
    print ("Please input the CIDR identifier of the target network (e.g. 192.168.1.0/24)")
    network = input("What is the CIDR?: ")
    networkAddress = network.split('/')[0]
    subnet = network.split('/')[1]

    # Get the user's list of ports to scan for each target
    ports = input("Please input a comma separated list of ports: ")
    ports = ports.split(',')

def Scan():
    '''
    Scan
    A function to iteratively scan ip address and port combination for testing.
    '''
    # Break the network address into octets for math reasons
    octets = networkAddress.split('.')
    # Calculate the subnet range
    subnet_range = 2**(32-int(subnet))
    # Create a list of tuples with the ranges for each octet
    octet_ranges = [[0,0],[0,0],[0,0],[0,0]]
    # Calculate the range of values for each octet
    for octet_range in reversed(range(0,4)):
        if (subnet_range-255 >= 0):
            octet_ranges[octet_range][0] = 0
            octet_ranges[octet_range][1] = 255
            subnet_range = subnet_range-256
        elif (subnet_range-255 < 0 and subnet_range > 0): 
            octet_ranges[octet_range][0] = 255-subnet_range
            octet_ranges[octet_range][1] = 255
            subnet_range = 0
        else:
            octet_ranges[octet_range][0] = octet_ranges[octet_range][1] = int(octets[octet_range])

    # For each range for each octet, build an IP address
    for octet1 in range(int(octet_ranges[0][0]), int(octet_ranges[0][1])+1):
        for octet2 in range(int(octet_ranges[1][0]), int(octet_ranges[1][1])+1):
            for octet3 in range(int(octet_ranges[2][0]), int(octet_ranges[2][1])+1):
                for octet4 in range(int(octet_ranges[3][0]), int(octet_ranges[3][1])+1):
                    address=[str(octet1), str(octet2), str(octet3), str(octet4)]
                    ip = '.'.join(address)
                    # For each port we want to scan, scan the target IP and report if it is open.
                    for port in ports:
                        # Uncomment the next line to get output for each address and port scanned. 
                        # print("Address: " + str(ip) + "\nPort: " + str(port))
                        if (TestSocket(str(ip), int(port))):
                            print ("Port " + str(port) + " is open on " + str(ip))
                        else: 
                            continue

# Run the program
GetInfo()
Scan()