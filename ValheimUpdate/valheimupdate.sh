#! /bin/bash
# Valheim Updater
# (c) Daelphinux 2021
# Released under the MIT License

# Verify the server is run as root
if [ $(id -u) -eq 0]
    then 
        # stop the service
        systemctl stop valheim.service

        # update the server via steam
        steamcmd +login anonymous +force_install_dir /home/valheim/valheimsrv +app_update 896660

        # restart the service
        systemctl start valheim.service
else
    echo "Please run with elevated privileges"
fi